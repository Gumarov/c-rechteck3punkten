﻿using System;
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;


namespace Re3P
{
    public class MyCommands
    {
        Document doc = Application.DocumentManager.MdiActiveDocument;
        Editor ed;
        Database db;
        [CommandMethod("Re3P")]
        public void MyCommand()
        {  
            if (doc != null)
            {
                ed = doc.Editor;
                db = doc.Database;
            }
            PromptPointOptions ppo1 = new PromptPointOptions("\n1.Punkt:");
            PromptPointResult ppr1 = ed.GetPoint(ppo1);
            if (ppr1.Status != PromptStatus.OK) return;
            Point3d point1 = ppr1.Value;
            PromptPointOptions ppo2 = new PromptPointOptions("\n2.Punkt:");
            PromptPointResult ppr2 = ed.GetPoint(ppo2);
            if (ppr2.Status != PromptStatus.OK) return;
            Point3d point2 = ppr2.Value;

            newUcs(point1, point2);

            PromptPointOptions ppo3 = new PromptPointOptions("\n3.Punkt:");
            PromptPointResult ppr3 = ed.GetPoint(ppo3);
            if (ppr3.Status != PromptStatus.OK)
            {
                ed.CurrentUserCoordinateSystem = Matrix3d.Identity;
                return;
            }
            Point3d point3 = ppr3.Value;

            Matrix3d ucs = ed.CurrentUserCoordinateSystem.Inverse();
            Matrix3d welt = ed.CurrentUserCoordinateSystem;
            //point1 = point1.TransformBy(ucs);

            using (Transaction acTrans = db.TransactionManager.StartTransaction())
            {
                BlockTable bt = acTrans.GetObject(db.BlockTableId,
                                                OpenMode.ForRead) as BlockTable;
                BlockTableRecord btr;
                btr = acTrans.GetObject(bt[BlockTableRecord.ModelSpace],
                                                OpenMode.ForWrite) as BlockTableRecord;

                using (Polyline myPoly = new Polyline())
                {
                    Point3d p1 = new Point3d(0, 0, 0).TransformBy(welt);
                    Point3d p2 = new Point3d(point3.X, 0, 0).TransformBy(welt);
                    Point3d p3 = new Point3d(point3.X, point3.Y, 0).TransformBy(welt);
                    Point3d p4 = new Point3d(0, point3.Y, 0).TransformBy(welt);

                    myPoly.AddVertexAt(0, new Point2d(p1.X, p1.Y), 0, 0, 0);
                    myPoly.AddVertexAt(1, new Point2d(p2.X, p2.Y), 0, 0, 0);
                    myPoly.AddVertexAt(2, new Point2d(p3.X, p3.Y), 0, 0, 0);
                    myPoly.AddVertexAt(3, new Point2d(p4.X, p4.Y), 0, 0, 0);
                    myPoly.Closed = true;

                    btr.AppendEntity(myPoly);
                    acTrans.AddNewlyCreatedDBObject(myPoly, true);
                }
                acTrans.Commit();
                ed.CurrentUserCoordinateSystem = Matrix3d.Identity;
            }
        }

        private void newUcs(Point3d point1, Point3d point2)
        {

            Transaction tr = db.TransactionManager.StartTransaction();
            Point3d[] myPunkt = new Point3d[2];
            Matrix3d UcsToWelt = ed.CurrentUserCoordinateSystem;
            using (tr)
            {
                UcsTable ut = tr.GetObject(db.UcsTableId, OpenMode.ForRead) as UcsTable;
                Point3d orig = new Point3d(point1.X, point1.Y, 0).TransformBy(UcsToWelt);
                Point3d x_point = new Point3d(point2.X, point2.Y, 0).TransformBy(UcsToWelt);
                Vector3d x_axis = orig.GetVectorTo(x_point);
                Vector3d y_axis = x_axis.GetPerpendicularVector();

                UcsTableRecord utr;

                if (ut.Has("Schacht") == false)
                {
                    utr = new UcsTableRecord();
                    utr.Name = "Schacht";
                    ut.UpgradeOpen();
                    ut.Add(utr);
                    tr.AddNewlyCreatedDBObject(utr, true);

                }
                else
                {
                    utr = tr.GetObject(ut["Schacht"], OpenMode.ForWrite) as UcsTableRecord;

                }
                utr.Origin = orig;
                utr.XAxis = x_axis;
                utr.YAxis = y_axis;
                Matrix3d myMat = new Matrix3d();
                myMat = Matrix3d.Displacement(new Vector3d(orig.X, orig.Y, 0));
                myMat *= Matrix3d.Rotation(new Vector2d((x_point.X - orig.X), (x_point.Y - orig.Y)).Angle,
                    new Vector3d(0, 0, 1), Point3d.Origin);
                ed.CurrentUserCoordinateSystem = myMat;
                tr.Commit();
            }
        }
    }

}
